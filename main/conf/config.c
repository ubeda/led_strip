/*
 * config.c
 *
 *  Created on: 12 nov. 2021
 *      Author: PC
 */

#include "../conf/config.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <stdio.h>
#include "time.h"
#include <time.h>
#include "string.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_attr.h"
#include "esp_sntp.h"


static const char *TAG = "CONF";
uint8_t my_sntp_initialized = 0;

/*
 * En config.c y config.h:
 * - RTC timer (reloj del ESP32)
 * - SNTP (hora real de internet)
 * - Timers
 */

void rtc_time_get_now(struct tm * time_now){

	memset(time_now, 0, sizeof(struct tm));
	time_t now;
	tzset();
	time(&now);
	localtime_r(&now, time_now);

}

//To set the timezone (TZ) function
void rtc_time_start_timezone(char * tz){

	if (getenv((char*)"TZ") == NULL) {
		ESP_LOGI(TAG, "Set timezone to: %s", tz);
		setenv("TZ", tz, 1);
	} else {
		ESP_LOGI(TAG, "Use timezone : %s", getenv("TZ"));
	}

}

//To get the current time broken down in a tm structure
void rtc_time_get_now_with_tz(struct tm * time_now){

	memset(time_now, 0, sizeof(struct tm));
	time_t now;
	tzset();
	time(&now);
	localtime_r(&now, time_now);

}

uint32_t rtc_time_get_epochtime(void){
	return time(NULL);
}

uint32_t rtc_time_get_epochtime_tz(void){

	time_t rawtime = time(NULL);			//Obtiene el tiempo actual en formato time_t y lo pega en "rawtime"
	struct tm *ptm = gmtime(&rawtime);		//Desglosa el tiempo en broken-down y lo pone en formato UTC (Coordinated Universal Time)
	return mktime(ptm);						//Normaliza la estructura de tiempo broken-down y lo devuelve

}

void rtc_time_print_tm(char * txt, struct tm * time){

	char buffer[64];

	strftime(buffer, 64, "%d/%m/%Y %H:%M:%S", time);

	ESP_LOGI(TAG, "%s%s", txt, buffer);

}

uint16_t rtc_time_get_ascii(char * buf, uint16_t buf_max){

	time_t now;
	struct tm timeinfo;
	memset(&now, 0, sizeof(time_t));
	memset(&timeinfo, 0, sizeof(struct tm));
	tzset();
	time(&now);
	localtime_r(&now, &timeinfo);
	return strftime(buf, buf_max, "%d/%m/%Y %H:%M:%S", &timeinfo);

}

void rtc_time_print(char * txt){

	char buffer[64];

	rtc_time_get_ascii(buffer, 64);		//Obtiene la hora local

	ESP_LOGI(TAG, "%s%s", txt, buffer);  //Imprimer la string que le paso seguido de la hora local obtenida

}

void rtc_time_set_time(struct tm * time_now){

	struct timeval tv;
	memset(&tv, 0, sizeof(struct timeval));

	tv.tv_sec = mktime(time_now);

	settimeofday(&tv, NULL);

}

void rtc_time_set_time_epoch(uint32_t time){

	struct timeval tv;
	memset(&tv, 0, sizeof(struct timeval));

	tv.tv_sec = time;

	settimeofday(&tv, NULL);

}

void rtc_time_set_time_epoch_with_tz(uint32_t time, char * tz){

	setenv("TZ", "UTC0", 1);
	tzset();

	rtc_time_set_time_epoch(time);

	setenv("TZ", tz, 1);
	tzset();

}

void rtc_time_set_time_with_tz(struct tm * time_now, char * tz){

	setenv("TZ", "UTC0", 1);
	tzset();

	rtc_time_set_time(time_now);

	setenv("TZ", tz, 1);
	tzset();

}

void sntp_sync_time(struct timeval *tv)
{
	settimeofday(tv, NULL);
	//ESP_LOGW(TAG, "Time is synchronized from custom code");
	sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
}

void time_sync_notification_cb(struct timeval *tv)
{
	ESP_LOGI(TAG, "Notification of a time synchronization event");
}

void sntp_wrap_init(void)
{

	if(my_sntp_initialized) return;

	ESP_LOGI(TAG, "Initializing SNTP");
	sntp_setoperatingmode(SNTP_OPMODE_POLL);
	sntp_setservername(0, "pool.ntp.org");
	sntp_set_time_sync_notification_cb(time_sync_notification_cb);
	sntp_init();

	my_sntp_initialized = 1;

}

void sntp_wrap_stop(void){
	ESP_LOGE(TAG, "SNTP stop not implemented");
}


