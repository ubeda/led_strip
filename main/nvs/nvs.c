/*
 * dismuntelNVF.c
 *
 *  Created on: 26 nov. 2020
 *      Author: dismuntel
 */
#include "nvs.h"
#include "nvs_flash.h"
#include "string.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#include "esp_app_trace.h"
//#include "esp_http_client.h"
//#include "esp_https_ota.h"
#include "freertos/task.h"

#include "nvs.h"
#include "parse_json.h"

#define TAG "NVF"

int (*onSetToDefault)(char* dataname, void* data, int datalenght);

void initNVS(int (*functionPtrSetParamToDefault)(char* dataname, void* data, int datalenght)) {
	onSetToDefault=functionPtrSetParamToDefault;
    // Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES)  { //En versiones mas nuevas  || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
}

void* readNVS(char* dataname,void *data, int datalenght) {
    // Open
	void* buffer=NULL;
    nvs_handle my_handle;
    esp_err_t err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) {
    	ESP_LOGI(TAG,"Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
        (*onSetToDefault)(dataname,data, datalenght);
    } else {
        // Read
    	ESP_LOGI(TAG,"Reading restart counter from NVS ... ");
        // Read the size of memory space required for blob
        size_t required_size = 0;  // value will default to 0, if not set yet in NVS
        err = nvs_get_blob(my_handle, dataname, NULL, &required_size);
        if (err == ESP_OK) {
        	if (required_size>0) {
        		if (required_size==datalenght) {
        			buffer=data;
        			err=nvs_get_blob(my_handle, dataname, data, &required_size);
        		} else {
        			if (required_size<datalenght) {
        				buffer=malloc(datalenght);
        				memset(buffer, 0, datalenght);
        				(*onSetToDefault)(dataname,buffer,datalenght);
        				if (data!=NULL) memcpy(data,buffer,datalenght);
        				err=nvs_get_blob(my_handle, dataname, data, &required_size);
        			} else {
        				buffer=malloc(required_size);
        				err=nvs_get_blob(my_handle, dataname, buffer, &required_size);
        				if (data!=NULL) memcpy(data,buffer,datalenght);
        			}
        			if ((data!=NULL)&&(buffer!=NULL)) {
        				free(buffer);
        				buffer=data;
        			}
        		}
       			if (err != ESP_OK) ESP_LOGE(TAG,"Problema al leer finalmente los datos (%s)", esp_err_to_name(err));
        	}
        } else {
        	(*onSetToDefault)(dataname,data,datalenght);
        	ESP_LOGE(TAG,"Error (%s) reading!\n", esp_err_to_name(err));
        }

        // Close (solo si ha habido open con exito)
        nvs_close(my_handle);
    }
    return buffer;
}

esp_err_t writeNVS(char* dataname, void* data, int datalenght) {
    nvs_handle my_handle;
    esp_err_t err;

    // Open
    err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    // Write
    size_t required_size = datalenght;
    err = nvs_set_blob(my_handle, dataname, data, required_size);
    if (err != ESP_OK) return err;
    // Commit
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;

    // Close
    nvs_close(my_handle);

    ESP_LOGW(TAG,"WriteNVS successful");

    return ESP_OK;
}

esp_err_t lanzarActualizacionOTA(char *conf) {
	int len=strlen(conf);
    if (len>4096) {
    	ESP_LOGE(TAG,"Datos excesivos %d > 4096\n", len);
    	return -1;
    }
    cJSON *root= cJSON_Parse(conf);
    if (root!=NULL)
      if (writeNVS("OTA", conf, strlen(conf))==ESP_OK) {
    	ESP_LOGI(TAG,"Listo para ACTUALIZACION OTA");
    	esp_restart();
      }
    return ESP_OK;
}

cJSON *root=NULL;

