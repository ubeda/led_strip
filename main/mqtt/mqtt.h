/*
 * mqtt.h
 *
 *  Created on: 5 oct. 2019
 *      Author: rafa400
 */

#ifndef MAIN_MQTT_H_
#define MAIN_MQTT_H_

#include "mqtt_client.h"

extern const char *nuestromqttTSL;
extern bool MQTTconectado;
char* getMACstr(char* macstr);
void printHEXX(uint8_t *buf,int n,char separador);

char* setTopicAndMAC(const char* topicall);
char* setTopicFin(const char* topicfin);
void task_MQTT(void *pvParameter);
void init_MQTT(esp_mqtt_client_config_t* mqtt_cfg, int (*functionPtr)(char* data),int (*functionPtrOnConnected)());
bool sendMQTT(char* topic,char* message);
bool hayMQTT();
void configuraMQTT(void);

#endif /* MAIN_MQTT_H_ */
