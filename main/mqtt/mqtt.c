/*
 * mqtt.c
 *
 *  Created on: 5 oct. 2019
 *      Author: rafa400
 */
#include "stdbool.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include "mqtt_client.h"
#include "mqtt.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_err.h"

static const char *TAG = "mqtt";

#define mqttmaxlen (1024*5)
int mqttlen=0;
char *mqttbuffer; //[mqttmaxlen];

esp_mqtt_client_handle_t client=NULL;
esp_mqtt_client_config_t mqtt_cfg; //Create the MQTT configuration structure

int callback_MQTT(char *data);

bool MQTTconectado=false;

char* getMAC(char* macstr) {
    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);
	snprintf(macstr,20,"%02x:%02x:%02x:%02x:%02x:%02x", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]+2);
	macstr[17]=0;
	// ESP_LOGW(jsonconfigTAG, "%s",macstr);
	return macstr;
}

// Recibe un char* con espacio para escribir la MAC en formato texto.
// Se obtiene la MAC asociada al BT/BLE

char* getMACstr(char* macstr) {
    uint8_t mac[6];
	memset(mac, 0, 6);
    esp_efuse_mac_get_default(mac);
    mac[5] = mac[5] + 0; //Get MAC
	snprintf(macstr,20,"%02X%02X%02X%02X%02X%02X", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	return macstr;
}

void printHEXX(uint8_t *buf,int n,char separador) {
    for (int i=0;i<n;i++) {
      printf ("%02X ",buf[i]);
        if (separador>0) if (i%8==7) printf ("%c ",separador);
    }
    if (n>0) printf ("\n");
}

const char *nuestromqttTSL =
		"-----BEGIN CERTIFICATE-----\n"
		"MIID7TCCAtWgAwIBAgIUTC9hZbdMaQxP2SFtieuoa9xQlTowDQYJKoZIhvcNAQEL\n"
		"BQAwgYwxFzAVBgNVBAMMDmFnY2NvbnRyb2wuY29tMRswGQYJKoZIhvcNAQkBFgxh\n"
		"ZG1pbkBjYS5jb20xEjAQBgNVBAsMCURpc211bnRlbDESMBAGA1UECgwJQ0FMSVhU\n"
		"ZWNoMREwDwYDVQQHDAhWYWxlbmNpYTEMMAoGA1UECAwDVkxDMQswCQYDVQQGEwJF\n"
		"UzAgFw0yMDA3MDkwOTE3NTlaGA8yMDk5MDYyMDA5MTc1OVowgZAxFzAVBgNVBAMM\n"
		"DmFnY2NvbnRyb2wuY29tMR8wHQYJKoZIhvcNAQkBFhBhZG1pbkBjbGllbnQuY29t\n"
		"MRIwEAYDVQQLDAlEaXNtdW50ZWwxEjAQBgNVBAoMCUNBTElYVGVjaDERMA8GA1UE\n"
		"BwwIVmFsZW5jaWExDDAKBgNVBAgMA1ZMQzELMAkGA1UEBhMCRVMwggEiMA0GCSqG\n"
		"SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDUaOgNgwZWpJI18dUomOZTVR5cypOVRHSV\n"
		"X2Xy691dq33QuYxjuKX2lsMePrwxxOExdWHZIkjnsNJUk/fPy7dwyPrlyIuLyilu\n"
		"PaHPv1yLtbcohRTBs/vpB5p9nuq6wPhRqMUvqbv6WVMy1FfGprJ0G08F27YAlScU\n"
		"xnNX6b/zPhvT2+jbnoy+8o7pXVOTEm0KLTpkP63aH0UrprsT/nfG3kyKEuEUUF2b\n"
		"SsSJoW45NvHtdMOiYoWamTA9OOCxT7Qy9YxoToo+SMWTuMACRqWnhJiBDYtzT3QU\n"
		"3jmQAv78pw4tvKsh/vo57iPLys3Ua5wKF1fz0yp6r9Wr8DAEBoPBAgMBAAGjPzA9\n"
		"MDsGA1UdEQQ0MDKCDjUxLjI1NS4xMjYuMjQzghAqLmFnY2NvbnRyb2wuY29tgg5h\n"
		"Z2Njb250cm9sLmNvbTANBgkqhkiG9w0BAQsFAAOCAQEAKUDibugwjeE6u/LZdqI4\n"
		"in1je0HRHwcU2iWIyDHz0XtslUgSKlCBVAqIvaYc120wszTeeJFfHKr1i3i+tIiv\n"
		"vy3eGmOJO3YTLwvoqkDpkcNd9CxkEqETfrfNTvdvjlHnyhO4GUW2hnr9Xk9MmTMa\n"
		"ppH0CbGt/xda2dDxE8sFhJQgwHuvvJv5Nl7KoU9b2sxzy+ou14hhfNGryBWZ//C/\n"
		"z0fGsqhBz/doJy5y+C1dVQCMM7hLPPgHo6Z5KrrcUYrtZz4P+29pVEo1CpjRtNIJ\n"
		"vco/gDPDQgGGlY3q27hWBs0uiudPaJWlIb44Mo9yfemn5ugupsGoqlne5N5F3vCI\n"
		"Mw==\n"
		"-----END CERTIFICATE-----\n"
		"-----BEGIN CERTIFICATE-----\n"
		"MIID/TCCAuWgAwIBAgIUMUXr3oLueyHmdH70KJJo6PhJmqIwDQYJKoZIhvcNAQEL\n"
		"BQAwgYwxFzAVBgNVBAMMDmFnY2NvbnRyb2wuY29tMRswGQYJKoZIhvcNAQkBFgxh\n"
		"ZG1pbkBjYS5jb20xEjAQBgNVBAsMCURpc211bnRlbDESMBAGA1UECgwJQ0FMSVhU\n"
		"ZWNoMREwDwYDVQQHDAhWYWxlbmNpYTEMMAoGA1UECAwDVkxDMQswCQYDVQQGEwJF\n"
		"UzAgFw0yMDA3MDkwOTE3NThaGA8yMDk5MDYyMDA5MTc1OFowgYwxFzAVBgNVBAMM\n"
		"DmFnY2NvbnRyb2wuY29tMRswGQYJKoZIhvcNAQkBFgxhZG1pbkBjYS5jb20xEjAQ\n"
		"BgNVBAsMCURpc211bnRlbDESMBAGA1UECgwJQ0FMSVhUZWNoMREwDwYDVQQHDAhW\n"
		"YWxlbmNpYTEMMAoGA1UECAwDVkxDMQswCQYDVQQGEwJFUzCCASIwDQYJKoZIhvcN\n"
		"AQEBBQADggEPADCCAQoCggEBAMkjDweC+Am1g/nKS0uWKuDrbnXCBaJZzz21AUVn\n"
		"nFMnLcQ1FBLTBOK44puHIlX3d4DHeex5gZs/nENaKf3FYchaBF8PyfJX+YMgLIWV\n"
		"CUtAMej1zI+7vC8m52w00e0cATJ5W4pZoSuU9SRnA1MzOYWl5FFhO4ICjMtTIcdC\n"
		"xMX/AG++rbbQMq18mcI1R5knFnqDsnYHKGnRLOoC5qLDtepIqKJJvVOhsFVE4/Qo\n"
		"mQ7Y4m+XHhQ0lTe5wtiFtW+31rqdLnvILHEAps0WzLl7UjQgpl8IGRm95EBR5AYK\n"
		"wa+KbHctDrn7H1+xWHyuBnCU7auVGENjtxx0xAkpsdlhdi8CAwEAAaNTMFEwHQYD\n"
		"VR0OBBYEFLlrQWZS0TO3c812Gj56gY/vfX4MMB8GA1UdIwQYMBaAFLlrQWZS0TO3\n"
		"c812Gj56gY/vfX4MMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEB\n"
		"AK/2Cc0BqrZOwpvTqdlZLCC0vwT8GUbpmZCqY4qsLq7tt4DH33R6BMewAYj18vIx\n"
		"AFxoM9sRVWBD/AUeGcvV5Gs8YrSm6kdlXGgWFCUWsZQnHSv5D9R2rEHo/vH47SgA\n"
		"wyxgQDm/LHkW2rACRQlnGqRye8mVMlXCTE8zsMFzSD/ddJJ5kNzNh7EuApxZqMyk\n"
		"h5ybPm5ojM5syV1WYO4m5+FaOYF7jEvMpi0E5yeMxPSXSRDJW1gVk2NQNtiv9USt\n"
		"rkL1TtdzGN7TvzKdgkxRJ7ONR7UaJAhlLREVloAmIH14lYJAMBpMSYT+DBJTzxn1\n"
		"jNCfKScWAVSPISW4avRAZQY=\n"
		"-----END CERTIFICATE-----\n";


int (*onRcvData)(char* data);
int (*onMQTTconnected)();

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    client = event->client;
    int msg_id=0;
    switch (event->event_id) {
    	case MQTT_EVENT_BEFORE_CONNECT:
    		break;
        case MQTT_EVENT_CONNECTED:
        	MQTTconectado=true;
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");

            char *topicMQTT;
            topicMQTT = setTopicFin("COMANDO");
            ESP_LOGI(TAG, ">>>>>>>>>>>>>>>>%s",topicMQTT);
            msg_id = esp_mqtt_client_subscribe(client, topicMQTT, 0);

            topicMQTT = setTopicFin("LED");
            ESP_LOGI(TAG, ">>>>>>>>>>>>>>>>%s",topicMQTT);
            msg_id = esp_mqtt_client_subscribe(client, topicMQTT, 0);

            if (onMQTTconnected!=NULL)
            	(*onMQTTconnected)();
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_DISCONNECTED:
        	MQTTconectado=false;
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA: {

            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            if (mqttlen==0) mqttbuffer=malloc(mqttmaxlen+1);
            mqttlen+=event->data_len;
            memcpy(&mqttbuffer[event->current_data_offset],event->data,event->data_len);
            if ((mqttlen>event->total_data_len) || (event->total_data_len>mqttmaxlen)) {
            	ESP_LOGE(TAG, "MQTT_data too long");
            	mqttlen=0;
            	free(mqttbuffer);
            } else if (mqttlen==event->total_data_len) {
            	mqttbuffer[event->total_data_len]=0;
            	mqttlen=0;
            	//entradaJSON(mqttbuffer);
            	if (onRcvData!=NULL)
            		(*onRcvData)(mqttbuffer);
            	free(mqttbuffer);
            } else {
            	ESP_LOGI(TAG,"Recibido");
            	if (event->current_data_offset==0) mqttlen=event->data_len;
            }
            }
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        case MQTT_EVENT_ANY:
        	ESP_LOGI(TAG, "MQTT_EVENT_ANY");
        	break;
        case MQTT_EVENT_DELETED:
        	ESP_LOGI(TAG,"MQTT_EVENT_DELETED");
    }
    return ESP_OK;
}

//Deprecated
void task_MQTT(void *pvParameter)
{
	//char* mqtt_temp_uri="mqtt://192.168.4.1:1883";
	const char* mqtt_temp_uri="mqtts://51.255.126.243";

    esp_mqtt_client_config_t mqtt_cfg={};
    mqtt_cfg.event_handle = mqtt_event_handler;
    mqtt_cfg.username="QAIR_dismuntel!";
	mqtt_cfg.password="mJZ#juYS6qBGMA@Hd2@JAgwkTeyAASc3";
    mqtt_cfg.port=8883;
    mqtt_cfg.transport = MQTT_TRANSPORT_OVER_SSL;
    mqtt_cfg.cert_pem = nuestromqttTSL;
    mqtt_cfg.uri=mqtt_temp_uri;

    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(client);
    while(1) {
    	vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}

void init_MQTT(esp_mqtt_client_config_t* mqtt_cfg, int (*functionPtr)(char* data),int (*functionPtrOnConnected)()) //function = siEntraDataMQTT
{
	onRcvData=functionPtr; // Si entra con NULL no haremos gestión
	onMQTTconnected=functionPtrOnConnected;
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    mqtt_cfg->event_handle = mqtt_event_handler;
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(mqtt_cfg); //Creates mqtt client handle based on the configuration.
    esp_mqtt_client_start(client);   //Starts mqtt client with already created client handle.
}

char topic[80]; // calix/ozono/xxxxxxxxxxxx/
char* finaltopic;
char* setTopicAndMAC(const char* topicall) {
	strncpy(topic,topicall,80);
	finaltopic=strstr(topic, "xxxxxx");		//Busca las 12 x de la MAC a�n por definir
	if(finaltopic != NULL) {
		getMACstr(finaltopic);		//Convierte las 12 x en la MAC
		finaltopic+=12;		//Recorre las 12 posiciones de la MAC para ponerse a su derecha
		(*finaltopic)='/'; // arregla el 0 del final de cadena de la MAC
		finaltopic++;		//Se mueve uno a la derecha
		(*finaltopic)=0;	//Pone a 0 la posici�n a la derecha de la barra seguida de la MAC
	}
	finaltopic=topic+strlen(topic);
	return topic;
}
char* setTopicFin(const char* topicfin) {
	strcpy(finaltopic,topicfin);			//Copia "topicfin" a "finaltopic", que se copiar� justo a la parte derecha, no se machar� el contenido de "finaltopic"
    return topic;	//Devolvemos "topic", el cual contiene "finaltopic" al final, y el cual tiene la MAC seguido de "topicfin"
}

bool sendMQTT(char* topic,char* message) {
	if ( ! MQTTconectado ){
		ESP_LOGI(TAG, "MQTT not connected");
		return false;
	}
	if (client==NULL) {
		ESP_LOGI(TAG, "NULL detected, MQTT not started");
		return false;
	}

    if(esp_mqtt_client_publish(client, topic, message, 0, 0, 0) != -1) return true;
    else return false;

}


bool hayMQTT() {
    if (MQTTconectado) return true;
    else return false;
}

void configuraMQTT(void) { //Got IP. Very first time that it is connected to WiFi //Init MQTT connection

	//const char* mqtt_temp_uri="mqtt://io.adafruit.com";
	const char* mqtt_temp_uri="mqtt://test.mosquitto.org";

	//const char* mqtt_temp_uri="broker.hivemq.com";


    //mqtt_cfg.transport = MQTT_TRANSPORT_OVER_TCP;
    mqtt_cfg.uri=mqtt_temp_uri;
    mqtt_cfg.port=1883;

	setTopicAndMAC("munich_station/xxxxxxxxxxxx/");
	init_MQTT(&mqtt_cfg,callback_MQTT,NULL);

}


