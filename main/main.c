#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include <stdio.h>
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_log.h"

#include "wifi_station.h"
#include "parse_json.h"
#include "mqtt.h"
#include "config.h"

#include "my_led_strip/my_led_strip.h"

#define SLEEP_MS(X)		vTaskDelay(pdMS_TO_TICKS(X))
static const char *TAG = "MAIN";


esp_mqtt_client_config_t mqtt_cfg; //Create the MQTT configuration structure

/*
 * LED STRIP
 *
 * ESP_32 recibe topic: "munich_station/7C87CEE34654/LUX" con mensaje:
 *
 * {
  	  "cmd": "LED",
  	  "red": 0,
  	  "green": 0,
  	  "blue": 0
	}
	Para imprimir red, seria: r=0,g=255,b=255
 *
 */

int callback_MQTT(char *data){

	cJSON *root;

	root = checkJSON(data);
	if (root==NULL) ESP_LOGE("MAIN","MQTT: Root not created");

	char* cmd=NULL;
	uint32_t red = 0;
	uint32_t green = 0;
	uint32_t blue = 0;

    if ((cmd=getStringMyJSON(root,"cmd"))!=NULL) {
		   if (strcmp(cmd,"LED")==0){
			   red = getNumberMyJSON(root,"red");
			   green = getNumberMyJSON(root,"green");
			   blue = getNumberMyJSON(root,"blue");

			   ESP_LOGW(TAG,"Recibimos comando LED. r=%u , g=%u , b=%u",red,green,blue);

		       set_color(red, green, blue);
		   }
    }

	cJSON_Delete(root);

    return 0;
}

int setParamToDefault(char* dataname, void* data, int datalenght){

	if (strcmp("misparametros",dataname)==0) {
		ESP_LOGW("Param","Parametros Fabrica guardados!");
	}
	return 0;
}

void app_main(void)
{

	ESP_LOGI(TAG, "Main start up");

	initNVS(setParamToDefault);
    readNVS("misparametros",NULL, sizeof(int));

    wifi_init_sta();

	led_strip_init();

	while(1){
		SLEEP_MS(1000);
	}

}
