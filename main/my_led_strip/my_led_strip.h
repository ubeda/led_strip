/*
 * led_strip.h
 *
 *  Created on: 16 jun 2024
 *      Author: mateo
 */

#ifndef LED_STRIP_M_H
#define LED_STRIP_M_H

#include "driver/ledc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

// Function to initialize the LED strip
void led_strip_init(void);

// Function to set the color of the LED strip
void set_color(uint32_t r, uint32_t g, uint32_t b);

#endif // LED_STRIP_H
